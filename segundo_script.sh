#Segundo script
#Crear variables para directorios*
cd /home/ksaucedo
chmod 777 segundo_script.sh
#Descargar Tomcat
wget https://www-us.apache.org/dist/tomcat/tomcat-9/v9.0.31/bin/apache-tomcat-9.0.31.tar.gz 
#Desempaquetar
tar xf /home/ksaucedo/apache-tomcat-9*.tar.gz
#Crear variable de JAVA_HOME y agregar la ruta de Java
export JAVA_HOME="/opt/java/jdk1.8.0_24"
#Agregar path de Java
export PATH=$JAVA_HOME/bin:$PATH
#Revisar si el puerto está ocupado
#Moverse al directorio de apache y dar permisos a startup.sh
cd /home/ksaucedo/apache-tomcat-9.0.31/bin
chmod 777 startup.sh
nc -vz 45.33.20.155 8080
if [¿? -ne 0]
then 
    echo puerto ocupado
else
    echo puerto libre, iniciando apache
    ./startup.sh
#Iniciar apache
fi
    


